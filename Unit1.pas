unit Unit1;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants,
  System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls;

type
  TCapital = class(TForm)
    ed_nome: TEdit;
    ed_pop: TEdit;
    ed_exterritorial: TEdit;
    ListBox_p: TListBox;
    btn_enviar: TButton;
    btn_atualizar: TButton;
    btn_excluir: TButton;
    btn_salvar: TButton;
    ed_salvar: TEdit;
    btn_carregar: TButton;
    procedure btn_enviarClick(Sender: TObject);
    procedure btn_carregarClick(Sender: TObject);
    procedure btn_atualizarClick(Sender: TObject);
    procedure btn_salvarClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

  Tcap = class(TObject)
    nome: String;
    exterritorial: String;
    pop: String;
  end;

var
  cap: Tcap;
  form1: TCapital;
  arquivo: TextFile;

implementation

{$R *.dfm}

procedure TCapital.btn_atualizarClick(Sender: TObject);
var
  minhaCapital: Tcap;
begin
  if ListBox_p.ItemIndex = 0 then
  begin
    ListBox_p.Items[ListBox_p.ItemIndex] := ed_nome.Text;

    minhaCapital := ListBox_p.Items.Objects[ListBox_p.ItemIndex] as Tcap;

    minhaCapital.nome := ed_nome.Text;
    minhaCapital.exterritorial := ed_exterritorial.Text;
    minhaCapital.pop := ed_pop.Text;

    ListBox_p.Items.Objects[ListBox_p.ItemIndex] := minhaCapital;

  end;

end;

procedure TCapital.btn_carregarClick
(Sender: TObject); begin AssignFile
(arquivo, 'Capital.txt'); Reset
(arquivo);

cap := Tcap.Create
(); cap.nome := ed_nome.Text; cap.exterritorial := ed_exterritorial.Text;
cap.pop := ed_pop.Text;

Showmessage
(cap.nome); Showmessage
(cap.exterritorial); Showmessage
(cap.pop); end;

procedure TCapital.btn_enviarClick
(Sender: TObject); begin if
(ed_nome.Text = '') or (ed_exterritorial.Text = '') or (ed_pop.Text = '')
then begin Showmessage('Preencha todos do campos');
end else begin cap := Tcap.Create
(); cap.nome := ed_nome.Text; cap.exterritorial := ed_exterritorial.Text;
cap.pop := ed_pop.Text;

ListBox_p.Items.AddObject
(cap.nome, cap); end;

end;

procedure TCapital.btn_salvarClick(Sender: TObject);
begin
  AssignFile (Arquivo, ed_salvar.Text + '.txt');

  Rewrite(Arquivo);

  Writeln (Arquivo, cap.nome);
  Writeln (Arquivo, cap.pop) ;
  Writeln (Arquivo, cap.exterritorial)  ;

  CloseFile(Arquivo) ;
  ShowMessage ('SALVO COM SUCESSO');
end;

end.
